$(document).ready(function () {
	var $jours = $('.jour'); 
	var $puces = $('.bullets .entypo-record		');

	function init() {
		setTimeout(function () {
			$('body').addClass('isok');
			$jours.hide();
			$('.wrapper').fadeIn('slow', function () {
				$jours.first().fadeIn('slow');
				// manque de parenthese pour la methode first
				$puces.removeClass('active').first().addClass('active');
			});
		}, 2000);

	} 
	// manque un 's' pour le varibale puce
	$puces.click(function () {
		var $this = $(this); var cible = $this.attr('data-cible');
		$jours.hide();

		$($jours.get(cible)).fadeIn()
		$puces.removeClass('active'); $this.addClass('active');
	});
	init(); 
// manque de }
}
);  